<html>
	<body>
		<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
		<jsp:useBean id="dao" class ="vn.edu.vnuk.record.dao.MotorbikeDao"></jsp:useBean>
		
		<c:import url="header.jsp"></c:import>
		
		<table>
			<c:forEach var= "motorbike" items= "${dao.read()}">
					  
				<tr>
					<td>${motorbike.brand}</td>
					<td>${motorbike.model}</td>
					<td>${motorbike.engineSize}</td>
					<td>${motorbike.gearBox}</td>
				</tr>
			
			</c:forEach>
		</table>
		<c:import url="footer.jsp"></c:import>
	</body>
</html>