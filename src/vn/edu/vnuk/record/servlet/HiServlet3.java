package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hello")
public class HiServlet3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		log("Starting servlet");
	}
	
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
		log("Ending servlet");
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException
	{
		
		PrintWriter out = respone.getWriter();
		out.println("<html>");
			
			out.println("<head>");
				
				out.println("<title> Project vnuk-record </title>");
			
			out.println("</head>");
			
			out.println("<body>");
				
				out.println("<h1> Out first servlet3 </h1>");
			
			out.println("</body>");
		
		out.println("</html>");
	}
	

}
