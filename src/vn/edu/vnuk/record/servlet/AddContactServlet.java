package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.model.Contact;

@WebServlet("/addContact")
public class AddContactServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException
	{	
		PrintWriter out = respone.getWriter();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String dateAsString = request.getParameter("date_of_birth");
		Calendar dateOfBirth = null;
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateAsString);
			dateOfBirth = Calendar.getInstance();
			dateOfBirth.setTime(date);
		}catch(ParseException e) {
			out.println("arror while converting date of birth");
			return;
		}
		Contact contact = new Contact();
		contact.setName(name);
		contact.setEmail(email);
		contact.setAddress(address);
		contact.setDateOfBirth(dateOfBirth);
		ContactDao dao = new ContactDao();
		try {
			dao.create(contact);
			out.println("<html>");
			
			out.println("<head>");
				
				out.println("<title> Project vnuk-record </title>");
			
			out.println("</head>");
			
			out.println("<body>");
				
				out.println("<h1> done! </h1>");
			
			out.println("</body>");
	
		out.println("</html>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
