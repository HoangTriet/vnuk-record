package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import vn.edu.vnuk.record.dao.MotorbikeDao;

import vn.edu.vnuk.record.model.Motorbike;

@WebServlet("/addMotorbike")
public class AddMotorbikeServlet  extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException
	{	
		PrintWriter out = respone.getWriter();
		String brand = request.getParameter("brand");
		String model = request.getParameter("model");
		String engineSize = request.getParameter("engine_size");
		String gearBox = request.getParameter("gear_box");
		int eS = Integer.parseInt(engineSize);
		
		
		Motorbike motorbike = new Motorbike();
		motorbike.setBrand(brand);
		motorbike.setModel(model);
		motorbike.setEngineSize(eS);
		motorbike.setGearBox(gearBox);
		MotorbikeDao dao = new MotorbikeDao();
		try {
			dao.create(motorbike);
			out.println("<html>");
			
			out.println("<head>");
				
				out.println("<title> Project vnuk-record </title>");
			
			out.println("</head>");
			
			out.println("<body>");
				
				out.println("<h1> done! </h1>");
			
			out.println("</body>");
	
		out.println("</html>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
